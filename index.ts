/**
 * replace `/` with `-`
 * @param v some string
 * @returns
 */
export function fileParse(v: string) {
  return v.split('/').join('-')
}